#include <stdio.h>
#include <stdlib.h>

#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
#include "led.pio.h"

// Declare LED functions
void led_init();

// Turns off the LED
void led_off();

// Set the color to red at half intensity
void led_red();

// Set the color to blue at half intensity
void led_blue();
// Set the color to green at half intensity
void led_green();

// Set the color to green at half intensity
void led_orange();