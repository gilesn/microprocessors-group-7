#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/watchdog.h"
#include "led.h"

#define MAX_MORSE_LENGTH 10 // max characters in strings


int lives, victory_count, current_level, right_input, wrong_input; // declare global variables
absolute_time_t start_time;

char current_input[MAX_MORSE_LENGTH], current_morse[MAX_MORSE_LENGTH], current_morse2[MAX_MORSE_LENGTH], current_word[MAX_MORSE_LENGTH];     // store current input value and morsecodes to be compared with

// Struct for storing each letter along with its morse
struct morse_letter {
    char letter;
    char morse_name[MAX_MORSE_LENGTH];
};

// Create array of all letters and their morse names
struct morse_letter rand_array[36];

// Set values for the morse combinations
void init_morse_array() {
    //letters
    rand_array[0].letter = 'A'; 
    strcpy(rand_array[0].morse_name, ".-");

rand_array[1].letter = 'B';
strcpy(rand_array[1].morse_name, "-...");

rand_array[2].letter = 'C';
strcpy(rand_array[2].morse_name, "-.-.");

rand_array[3].letter = 'D';
strcpy(rand_array[3].morse_name, "-..");

rand_array[4].letter = 'E';
strcpy(rand_array[4].morse_name, ".");

rand_array[5].letter = 'F';
strcpy(rand_array[5].morse_name, "..-.");

rand_array[6].letter = 'G';
strcpy(rand_array[6].morse_name, "--.");

rand_array[7].letter = 'H';
strcpy(rand_array[7].morse_name, "....");

rand_array[8].letter = 'I';
strcpy(rand_array[8].morse_name, "..");

rand_array[9].letter = 'J';
strcpy(rand_array[9].morse_name, ".---");

rand_array[10].letter = 'K';
strcpy(rand_array[10].morse_name, "-.-");

rand_array[11].letter = 'L';
strcpy(rand_array[11].morse_name, ".-..");

rand_array[12].letter = 'M';
strcpy(rand_array[12].morse_name, "--");

rand_array[13].letter = 'N';
strcpy(rand_array[13].morse_name, "-.");

rand_array[14].letter = 'O';
strcpy(rand_array[14].morse_name, "---");

rand_array[15].letter = 'P';
strcpy(rand_array[15].morse_name, ".--.");

rand_array[16].letter = 'Q';
strcpy(rand_array[16].morse_name, "--.-");

rand_array[17].letter = 'R';
strcpy(rand_array[17].morse_name, ".-.");

rand_array[18].letter = 'S';
strcpy(rand_array[18].morse_name, "...");

rand_array[19].letter = 'T';
strcpy(rand_array[19].morse_name, "-");

rand_array[20].letter = 'U';
strcpy(rand_array[20].morse_name, "..-");

rand_array[21].letter = 'V';
strcpy(rand_array[21].morse_name, "...-");

rand_array[22].letter = 'W';
strcpy(rand_array[22].morse_name, ".--");

rand_array[23].letter = 'X';
strcpy(rand_array[23].morse_name, "-..-");

rand_array[24].letter = 'Y';
strcpy(rand_array[24].morse_name, "-.--");

rand_array[25].letter = 'Z';
strcpy(rand_array[25].morse_name, "--..");

// Digits
rand_array[26].letter = '1';
strcpy(rand_array[26].morse_name, ".----");

rand_array[27].letter = '2';
strcpy(rand_array[27].morse_name, "..---");

rand_array[28].letter = '3';
strcpy(rand_array[28].morse_name, "...--");

rand_array[29].letter = '4';
strcpy(rand_array[29].morse_name, "....-");

rand_array[30].letter = '5';
strcpy(rand_array[30].morse_name, ".....");

rand_array[31].letter = '6';
strcpy(rand_array[31].morse_name, "-....");

rand_array[32].letter = '7';
strcpy(rand_array[32].morse_name, "--...");

rand_array[33].letter = '8';
strcpy(rand_array[33].morse_name, "---..");

rand_array[34].letter = '9';
strcpy(rand_array[34].morse_name, "----.");

rand_array[35].letter = '0';
strcpy(rand_array[35].morse_name, "-----");

}

void main_asm();

//------------------------ GPIO Initialization -----------------------------//
// Initialise a GPIO pin – see SDK for detail on gpio_init()
void asm_gpio_init(uint pin) {
    gpio_init(pin);
}

// Set direction of a GPIO pin – see SDK for detail on gpio_set_dir()
void asm_gpio_set_dir(uint pin, bool out) {
    gpio_set_dir(pin, out);
}

// Get the value of a GPIO pin – see SDK for detail on gpio_get()
bool asm_gpio_get(uint pin) {
    return gpio_get(pin);
}

// Set the value of a GPIO pin – see SDK for detail on gpio_put()
void asm_gpio_put(uint pin, bool value) {
    gpio_put(pin, value);
}

// Enable falling-edge interrupt – see SDK for detail on gpio_set_irq_enabled()
void asm_gpio_set_irq(uint pin) {
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_FALL, true);
}

// Stores current time
int start_timer() {
    start_time = get_absolute_time();
    return 0;
}

// Compares current to start time.
int end_timer() {
    return (int)absolute_time_diff_us(start_time, get_absolute_time());
}


//Adds a character to the morse string
void add_input(int morse_in) {

    int i;
    //get last element in current input
    for(i=0;current_input[i]!='\0';i++);

    //add new character

    //don't overflow array
    if(i>=MAX_MORSE_LENGTH-1)
        return;

    if(morse_in==0) current_input[i] = '.';
    else if(morse_in==1) current_input[i] = '-';
    else if(morse_in==2) current_input[i] = ' ';

    current_input[i+1] = '\0'; //null character


}

// Returns level corresponding to number
int level(int x) {
    return x;
}




// Inputting for various difficulty levels
// disp_morse: Displays the morse code
// letter: 0=word, 1=single letter
void get_level(int disp_morse, int letter) {

    char current_letter;
    char current_morse[MAX_MORSE_LENGTH];
    int num_count=rand()%36;
    if(letter==1){
        current_letter = rand_array[num_count].letter;
        //current_morse = rand_array[num_count]->morse_name;
        strcpy(current_morse,rand_array[num_count].morse_name);
        if(disp_morse==1){
            printf("\n\t\tYour Challenge is: %c \n\t\tand %c in Morse is %s:\n",current_letter,current_letter,current_morse);
        }else
            printf("\n\t\tYour challenge is: %c\n",current_letter);
    }
    else{
        for(int i=0;i<3;i++){
            current_word[i]=rand_array[num_count].letter;
            strcat(current_morse2,rand_array[num_count].morse_name);

                if(i!=2){
                    strcat(current_morse2," ");
                }
                num_count=rand()%36;
        }

        current_word[3]='\0';
        
        strcpy(current_morse2,current_morse);
        if(disp_morse==1){
            printf("\n\t\tYour Challenge is: %s \n\t\tand %s in Morse, it's %s:\n",current_word,current_word,current_morse);
        }
        else
            printf("\n\t\tYour Challenge is: %s\n",current_word);

    }

}
/*
Processing input dat:

Compare the Morse code of the input and data entered. If they are matched

        • Increase the number of correct sequences: victory_count++
        • Increase the number of lives if it is now less than 3 lives = lives + 1
        • If there are five correct sequences in a row, advance to the next level, 
          current_level = level(current_level+1)
        • If it is now level 4, ends game!!!


//If input not match
      • If the input is not matched
      • Reduce the number of lives, lives = lives – 1
      • Reset the number of correct sequences: victory_count = 0


*/
void process_input() {
    if(strcmp(current_morse,current_input)==0){
        right_input++;
        victory_count++;
        printf("\t\tCongrats, you have completed the Game!\n");

        if(lives<3)lives=lives+1;

        if(victory_count==5){

            if(current_level==4){
                printf("\t\tCongrats, you have completed the Game!\n");
                winning_sequence();
                return;
            }

            current_level=level(current_level+1);
            printf("\n\n\t\t#################################################################\n\n");
            printf("\t\t\t\tAdvancing to level %d\n\n",current_level);
            printf("\t\t##########################################################\n\n");
            calculateStats(1);
            victory_count=0;
        }
    }else{
        wrong_input++;
        printf("Incorrect :(\n");

        // Functionality to find corresponding name for morse code
        /*
        if(strcmp(hashTable[pos]->morse_name,"")==0){
            printf("\t\tMorse Code does not exist\n");
        }
        else if(letter){
            printf("\t\tMorse Code you entered is for %c\n",hashTable[pos]->letter);
        }
        */
        lives=lives-1;
        victory_count=0;
    }

    set_correct_led();
    current_input[0]='\0';
    current_morse2[0]='\0';
    if(right_input+wrong_input)calculateStats(0);

}

/*
Set_Correct_Led:
 Update the LED colours
    • 1 live: Orange
    • 2 lives: Yellow
    • 3 lives: Green
*/

void set_correct_led(){
    if(lives ==1)led_red();
    else if (lives==2)led_yellow();
    else led_green();
    return;
}

//Processing Input Data, part 3
/*
    • When the player is at the final level, show a suitable message
    • Exit the game if no input is entered or the Morse code of 2 "..---" is entered
    • Enter the Morse code of 1 () to play again, ".----"
    • Show the statistics of the game

*/
void game_end() {
    calculateStats(1);
    if(lives==0) led_red();
    printf("\n\n\n\n\n\n\t\t******************************\n");
    printf("\t\t*                           *\n");
    printf("\t\t* Enter .---- to play again *\n");
    printf("\t\t* Enter ..--- to exit       *\n");
    printf("\t\t*                           *\n");
    printf("\t\t*****************************\n");

    main_asm();

    if(current_input==0){
        printf("\t\tNo update detected\n\t\tProgram will now Exit.");
    }

    if(strcmp(current_input,".----")==0){
        start_the_game();
    }
}


//Processing Input Data, part 4
/*
    • Show the statistics of the game
    • Examples:
    • Total correct attempts
    • Correctness ratio
    • Statistics for individual levels and for the entire game
    • Individual levels: show the statistics when victory_count == 5
    • Entire game: current_level == 4 and victory_count == 5 or lives == 0
*/

/**
* @brief A founction called upon in the start of the game function that calculates
*        your overall accuracy throughout the game by summing your total attempts over
*        the total correct attempts.
*/

void calculateStats(int reset){
    printf("\n\n\t\t*****************Scoreboard*****************");
    printf("\n\t\t*Attempts: \t\t\t\t%d*",right_input+wrong_input);
    printf("\n\t\t*Correct: \t\t\t\t%d*",right_input);
    printf("\n\t\t*Incorrect \t\t\t\t%d*",wrong_input);
    printf("\n\t\t*Win Streak: \t\t\t\t%d*",victory_count);
    printf("\n\t\t*Lives Left: \t\t\t\t%d*\n",lives);
    if(right_input!=0||wrong_input!=0){
        float stat=right_input*100/(right_input+wrong_input);
        if(reset){
            right_input=0;
            wrong_input=0;
            printf("\t\t*Correct %% for this level: \t%.2f%%*\n",stat);
        }
        else{
            printf("\t\t*Correct Percent :\t\t\t%.2f%%\n",stat);
        }

    }
    printf("\t\t****************Scoreboard****************\n\n");



}

int main() {
    stdio_init_all();

    init_morse_array();
    
    lives = 3;
    victory_count = 0;
        
    //Starting the game: 1) Welcome Message

    //A welcome header saying morse code game

    printf("\n\n**********************************************************************************************************************\n");
    printf("| #               # # # # # # #           #          # # # # # # #      #                #                               |\n");
    printf("| #               #                     #   #        #            #     #  #             #                               |\n");
    printf("| #	              #                    #     #       #            #     #    #           #                               |\n"); 
    printf("| #		          #                   #       #      #       	  #     #      #         #                               |\n");
    printf("| # 	          # # # # # # #      # # # # # #     # # # # # # #      #        #       #                               |\n");
    printf("| #		          #                  #         #     #            #     #          #     #  	                         |\n");
    printf("| #		          #              	 #         #     #            #     #            #   #                               |\n");
    printf("| #		          #                  #         #     #            #     #              # #                 	             |\n");
    printf("| # # # # # # #	  # # # # # # #	     #         #     #            #     #                #                               |\n");
    printf("|													                                                                     |\n");	
    printf("|														                                                                 |\n");	
    printf("|														                                                                 |\n");	
    printf("|		        ..._.....===.////==========================================\\\\.===......_...			                 |\n");
    printf("|														                                                                 |\n");	
    printf("| #       #    "); printf("   # # # # # "); printf(" # # # # # # ");printf("  # # # # # ");  printf(" # # # # # # # ");
    printf("| # #   # #    "); printf(" #  	       #"); printf(" #		   # "); printf(" #	        ");  printf(" #	            ");	
    printf("| #  # #  #    "); printf(" #	       #"); printf(" #		   # "); printf(" #	         "); printf(" #             ");
    printf("| #       #    "); printf(" #	       #"); printf(" # # # # # # "); printf("  # # # # # "); printf(" # # # # # # # ");
    printf("| #	      #    "); printf(" #  	       #"); printf(" #		   # "); printf("		    #"); printf(" #             ");
    printf("| #	      #    "); printf(" #	       #"); printf(" #		   # "); printf("		    #"); printf(" #             ");
    printf("| #	      #    "); printf("  # # # # #  "); printf(" #		   # "); printf(" # # # # #  "); printf(" # # # # # # # ");	  

    //2)Group Number
    //Introduction to the game
    printf("\n\nMorse Code Game: Group 7");
    printf("\n\nHow to play game");
    printf("\nTo play, you must enter the correct morse code sequence");
    printf("\nfor the character or word displayed on the console.");
    printf("\nHold down GP21 for < 0.25s seconds for a dot  and > 0.25 seconds for a dash.");
    printf("\nLeave the button pressed for 1 second for a space and 2 seconds to submit the sequence.");
    printf("\nYou have 3 lives before the game is over, good luck.");

    //Select a difficulty level
    // There should be at least 2 levels at the
    // start of the game
    // Level 1: Individual characters and given
    // equivalent Morse code
    // Level 2: Without Morse code
    // Optional levels 3/4: Individual words	

    printf("\t\t*						  *\n");
    printf("\t\t* Enter .---- for level 1 (Characters - Easy) *\n");
    printf("\t\t* Enter ..--- for level 2 (Characters - Hard) *\n");
    printf("\t\t* Enter ...-- for level 3 (Words - Easy)  	  *\n");
    printf("\t\t* Enter ....- for level 4 (Words - Hard)  	  *\n");
    printf("\t\t*						                      *\n");
    printf("\t\t***********************************************\n");

    


    
    //Select a Difficulty
    printf("\n\n\n\n\n\n");
    
    if(strcmp(current_input,".----")==0){
        current_level=1;
    }else if(strcmp(current_input,"..---"==0)){
        current_level=2;
    }else if(strcmp(current_input,"...--")==0){
        current_level=3;
    }else if(strcmp(current_input,"....-")==0){
        current_level=4;
    }else{
        printf("\t\tWrong Input\n\t\tProgram will now exit\n");
        return;
    }

    //Main gameplay loop - get level, compare input, update LED
    for(;;) {
        get_level(current_level%2, current_level<3); // Display challenge
        main_asm(); // Call assembly to get morse input
        process_input(); // Check if input is correct
        if(lives==0)
            break;
        set_correct_led();  // Update LED according to game state
    }

    game_end();


    return 0;
}


